Date.prototype.addHours= function(h){
    this.setHours(this.getHours()+h);
    return this;
}

function load_data(resources, events){
    
    $('#calendar').fullCalendar({
        eventClick: function(calEvent, jsEvent, view) { 
            today = new Date();
            swal({
                title: calEvent.title,
                type: 'info',
                html: '<div align="left" style="padding:0px 30px 0px 30px">' + 
                    '<b>Correo: </b>' + calEvent.email +
                    '<br><b>Celular: </b>' + calEvent.celular +
                    '<br><b>Teléfono: </b>' + calEvent.phone +
                    '<br><b>Fecha: </b>' + calEvent.start._i +
                    '<br><br><b>Ultima actualizacion: </b>' + calEvent.description +
                    // '<br><br><b>Producto: </b>' + resources.find(function(res){ return res.id == calEvent.resourceId }).title +
                    '</div>',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonText: 'Agendar',
                cancelButtonText:  'Cerrar',
            }).then(function (name) {
                $('#input-arrange').val(calEvent.cod);
                $('#form-arrange').trigger('submit');                
            }, function (dismiss) {
                if (dismiss === 'cancel') {
                    
                }
            })
        },        
        
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        minTime: "07:00:00",
        maxTime: "19:00:00",
        allDaySlot: false,
            
        now: (new Date()).toISOString(),
        editable: false,
        aspectRatio: 1.8,
        scrollTime: '00:00',
        header: {
            left: 'today prev,next',
            center: 'title',
            right: 'timelineDay,timelineThreeDays,agendaWeek,month'
        },
        views: {
            timelineThreeDays: {
                type: 'timeline',
                duration: { days: 3 }
            }
        },
        //resourceGroupField: 'building',
        resources: resources,
        events: events,
    });
    
}

$(function(){
    
    $.post('/crm-calendario').done(function(data){
        
        info = data.rows;
        
        colours = [ 'gray', 'black', 'red', 'maroon', 'purple', 'olive', 'lime', 'green', 'aqua', 'teal', 'blue', 'navy', 'fuchsia', 'purple' ];
        resources = [];
        events = [];
        
        data.products.forEach(function(item, i){
            resources.push({
                id: item.value,
                building: item.text,
                title: item.text,
                eventColor: colours[i % 14]
            });
        })
        
        data.rows.forEach(function(item, i){
            events.push({
                id: '' + (i + 1), 
                resourceId: item.lista1, 
                start: (new Date(item.fecha1).addHours(2)).toISOString(),
                title: item.campo1,
                email: item.campo2,
                celular: item.campo3,
                phone: item.telefono,
                description: item.texto1,
                cod: item.cod
            })                        
        })
        
        load_data(resources, events);
           
    })
    
})