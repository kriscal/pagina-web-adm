


$( document ).ready(function() {
    console.log( "ready!" );
	// gestion de la fotografía 
//Codigo Global
g_cod=$('#global_cod').val()	
//Imagen 1	
var indeterminate1 = $('#contenedor_progress1');
var botonera1 = $('#btn_imagen1');
var input_imagen1 = $('#imagen1');
var btn_new1=$('#nueva_foto1')
var form_imagen1 = $('#form_imagen1');
var actualiza1 = $('#actualizar_foto1');
var cancelar1 = $('#cancelar_foto1');
var preview1 = $('#preview1');
//Imagen 2	
var indeterminate2 = $('#contenedor_progress2');
var botonera2 = $('#btn_imagen2');
var input_imagen2 = $('#imagen2');
var btn_new2=$('#nueva_foto2')
var form_imagen2 = $('#form_imagen2');
var actualiza2 = $('#actualizar_foto2');
var cancelar2 = $('#cancelar_foto2');
var preview2 = $('#preview2');
//Imagen 2	
var indeterminate3 = $('#contenedor_progress3');
var botonera3 = $('#btn_imagen3');
var input_imagen3 = $('#imagen3');
var btn_new3=$('#nueva_foto3')
var form_imagen3 = $('#form_imagen3');
var actualiza3 = $('#actualizar_foto3');
var cancelar3 = $('#cancelar_foto3');
var preview3 = $('#preview3');
//ocultar elementos iniciales
//imagen 1
botonera1.hide()
indeterminate1.hide()
//imagen 2
botonera2.hide()
indeterminate2.hide()
//imagen 3
botonera3.hide()
indeterminate3.hide()

//IMAGEN 1  CAMBIO
input_imagen1.change(function(data){
	
	indeterminate1.show();
	console.log(input_imagen1.val());
	formData = new FormData(form_imagen1[0]);
	console.log()
	$.ajax({
		  url: '/api/subir',
		  type: 'POST',
		  data: formData,
		  processData: false,
		  contentType: false,
		  success: function(data){
			  console.log('la ruta es ');
			  console.log(data);
			  preview1.attr("src", data);
			  botonera1.show();
			  indeterminate1.hide();
			  btn_new1.hide()
			  actualiza1.click(function(){
				  console.log('Actualizando Foto')
				  
				 botonera1.hide();
				 btn_new1.show()
				 $.get('/api/update-photo',{
					 index:"0",
					 data:data,
					 cod:g_cod
				 },function(dt){
					 console.log(dt)
					 
				 })
					 // $.post('/api/actualizar-foto',{foto:data},function(data){
					//  console.log(data)
					//  location.reload();
				 // }) ;
			  });
			  cancelar1.click(function(){
				 location.reload(); 
			  });
		  }
});
});
	
//IMAGEN 2 CAMBIO
input_imagen2.change(function(data){
	
	indeterminate2.show();
	console.log(input_imagen2.val());
	formData = new FormData(form_imagen2[0]);
	console.log()
	$.ajax({
		  url: '/api/subir',
		  type: 'POST',
		  data: formData,
		  processData: false,
		  contentType: false,
		  success: function(data){
			  console.log('la ruta es ');
			  console.log(data);
			  preview2.attr("src", data);
			  botonera2.show();
			  indeterminate2.hide();
			  btn_new2.hide()
			  actualiza2.click(function(){
				  console.log('Actualizando Foto')
				  
				 botonera2.hide();
				 btn_new2.show()
				 $.get('/api/update-photo',{
					 index:"1",
					 data:data,
					 cod:g_cod
				 },function(dt){
					 console.log(dt)
					 
				 })
					 // $.post('/api/actualizar-foto',{foto:data},function(data){
					//  console.log(data)
					//  location.reload();
				 // }) ;
			  });
			  cancelar2.click(function(){
				 location.reload(); 
			  });
		  }
});
});

//IMAGEN 3 CAMBIO
input_imagen3.change(function(data){
	
	indeterminate3.show();
	console.log(input_imagen3.val());
	formData = new FormData(form_imagen3[0]);
	console.log()
	$.ajax({
		  url: '/api/subir',
		  type: 'POST',
		  data: formData,
		  processData: false,
		  contentType: false,
		  success: function(data){
			  console.log('la ruta es ');
			  console.log(data);
			  preview3.attr("src", data);
			  botonera3.show();
			  indeterminate3.hide();
			  btn_new3.hide()
			  actualiza3.click(function(){
				  console.log('Actualizando Foto')
				  
				 botonera3.hide();
				 btn_new3.show()
				 $.get('/api/update-photo',{
					 index:"2",
					 data:data,
					 cod:g_cod
				 },function(dt){
					 console.log(dt)
					 
				 })
					 // $.post('/api/actualizar-foto',{foto:data},function(data){
					//  console.log(data)
					//  location.reload();
				 // }) ;
			  });
			  cancelar3.click(function(){
				 location.reload(); 
			  });
		  }
});
});

});

$('#btn1').click(function(){
	$.get('/api/update-category',{
		index:"0",
		data:$('#cat1').val(),
		cod:g_cod
	}, function(dt){
		Swal.fire(
			'Actualizado',
			'',
			'success'
		  )
		console.log(dt)
	})
})

$('#btn2').click(function(){
	$.get('/api/update-category',{
		index:"1",
		data:$('#cat2').val(),
		cod:g_cod
	}, function(dt){
		Swal.fire(
			'Actualizado',
			'',
			'success'
		  )
		console.log(dt)
	})
})

$('#btn3').click(function(){
	$.get('/api/update-category',{
		index:"2",
		data:$('#cat3').val(),
		cod:g_cod
	}, function(dt){
		Swal.fire(
			'Actualizado',
			'',
			'success'
		  )
		console.log(dt)
	})
})


$('#btn4').click(function(){
	$.get('/api/update-category',{
		index:"3",
		data:$('#cat4').val(),
		cod:g_cod
	}, function(dt){
		Swal.fire(
			'Actualizado',
			'',
			'success'
		  )
		console.log(dt)
	})
})

