$(function(){
    var table = $('#data-1').DataTable();
    var client_table = $('#data-2').DataTable();
    $.post('/crm-analizar').done(function(dt) {               
        categories = [];
        series = [[], [], [], []];        
        for(i = 0; i < dt.products.length; i ++){
            categories.push(dt.products[i].text);
            for(j = 0; j < 4; j ++)
                series[j][i] = 0;
        }        
        for(i = 0; i < dt.products.length; i++)
            for(j = 0; j < 4; j ++)
                for(k = 0; k < dt.rows[j].length; k++)
                    if(parseInt(dt.rows[j][k].lista1) == 1)
                        series[j][i] ++;
        Highcharts.chart('container', {
            chart: {
            type: 'bar'
            },
            title: {
                text: 'Áreas en observación'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Peticiones registradas'
                }
            },
            legend: {
                reversed: true
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            series: [{
                name: 'Ingresado',
                data: series[0]
            }, {
                name: 'Procesando',
                data: series[1]
            }, {
                name: 'Aceptado',
                data: series[2]
            }, {
                name: 'Rechazado',
                data: series[3]
            }]
        });
        table.destroy();
        table = $('#data-1').DataTable({
            data: dt.rows[0].concat(dt.rows[1]).concat(dt.rows[2]).concat(dt.rows[3]),
            "columns": [
                {"data": function(row){ 
                    value = 'rechazado';
                    if(row.est == '1' || row.est == '5')
                        value = 'ingresado';
                    if(row.est == '2' || row.est == '6')
                        value = 'procesando';
                    if(row.est == '3' || row.est == '7')
                        value = 'aprobado';
                    return value;
                }},
                {"data": "campo1" },
                {"data": "campo2" },
                {"data": "campo3" },
                {"data": function(row){
                    return dt.products[0].text;
                }},
                {"data": function(row){
                    return row.fecha1.substring(0,10);
                }},
                {"data": "texto1" },
                {"data": "ven" },
                {"data": "valor_producto" },
                {"defaultContent": "<a href='#data' class='btn-floating waves-effect waves-light light-blue view'><i class='material-icons'>search</i></a>"}
            ]
        });
        table.column( 7 ).visible( false );
        $('#data-1 tbody').on("click", "td .view", function(){
            $('#info-2').css('display','none');
            data = { cod: table.row( $(this).parents('tr') ).data().cod }
            $.post('/crm-cliente', data).done(function(obj) {
                $('#info-name').html(obj.rows[0].campo1);
                $('#info-mail').html(obj.rows[0].campo2);
                $('#info-phon').html(obj.rows[0].campo3);
                $('#info-prod').html('Cobranzas')
                client_table.destroy();
                client_table = $('#data-2').DataTable({
                    data: obj.rows,
                    "columns": [
                        {"data": function(row){ 
                            return row.fecha1.substring(0,10);
                        }},
                        {"data": function(row){ 
                            value = 'rechazado';
                            if(row.est == '1' || row.est == '5')
                                value = 'ingresado';
                            if(row.est == '2' || row.est == '6')
                                value = 'procesando';
                            if(row.est == '3' || row.est == '7')
                                value = 'aprobado';
                            return value;
                        }},
                        {"data": "texto1" },
                        {"data": "ven" },
                        {"data": "valor_producto" },
                    ]
                });
                $('select').material_select();
                $('#info-2').css('display','block');
            });
        });
        $('#btn-less').on('click', function(){
            $('#info-2').css('display','none');
        })
        $('select').material_select();
    });
    // var tabledata1 = $('#data-1').tableExport({
    //     ignoreCols: [7, 8],
    //     bootstrap: false,
    //     formats: ['xlsx', 'xls', 'csv', 'txt'],
    //     position: 'bottom'
    // });
    // var tabledata2 = $('#data-2').tableExport({
    //     bootstrap: false,
    //     formats: ['xlsx', 'xls', 'csv', 'txt'],
    //     position: 'bottom'
    // });
    // table.on( 'draw', function () {
    //     tabledata1.reset();
    // });    
    // client_table.on( 'draw', function(){
    //     tabledata2.reset();
    // });     
})