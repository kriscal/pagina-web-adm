$(document).ready(function() {
    $('select').material_select();
});

$(function(){
	$('.datepicker').pickadate({
       selectMonths: true,
       selectYears: 15,
	   format: 'yyyy-mm-dd'
	});	
    $('#form-arrange').on('submit', function(e){
        if($('#fecha1').val() == ''){
            alert('Debe ingresar una fecha valida');
            e.preventDefault();
        }
    })
});

$('#boton-actualizar').hide(); 
$('#pago').change(function(){
    var valor =$('#pago').is(":checked");
    $('#pago1').val(valor);
    if($('#pago1').val() ==  "true"){
        $('#formulario').hide();
        $('#boton-actualizar').show(); 
    }else{
        $('#formulario').show(); 
        $('#boton-actualizar').hide(); 
    }
})

