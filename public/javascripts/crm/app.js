$(window).resize(function(){
	col_height = $(window).height() - $('.page-footer').height() - $('.navbar-fixed').height() - 20;
	$('#div1').height(col_height);
	$('#div2').height(col_height);
	$('#div3').height(col_height);
	$('#div4').height(col_height);	
});

$(function(){   
    if($('#post-message').val() != ''){
        Materialize.toast($('#post-message').val(), 5000);
    }
    col_height = $(window).height() - $('.page-footer').height() - $('.navbar-fixed').height() - 20;
	$('#div1').height(col_height);
	$('#div2').height(col_height);
	$('#div3').height(col_height);
	$('#div4').height(col_height);
    $('ul.collection').draggable({
        helper: 'clone',
		start: function(e, ui){
            ui.helper.width($(this).width());
			$(this).hide();
        },
        stop: function(e, ui){
            $(this).show();
        },
		revert: 'invalid',
		zIndex: "7000"
    });
    $("#div2").droppable({
        accept: "#col_1 ul",
        drop: function(e, ui){
            $('#col_2').append(ui.draggable);
            arrange(ui.draggable[0].id);
        }
    });
    $("#div3").droppable({
        accept: "#col_2 ul",
        drop: function(e, ui){
            $('#col_3').append(ui.draggable);
            accept(ui.draggable[0].id);
        }
    });
    $("#div4").droppable({
        accept: "#col_2 ul",
        drop: function(e, ui){
            $('#col_4').append(ui.draggable);
            reject(ui.draggable[0].id);
        }
    });
    $('.actions').on('click', function(){
        $('#input-actions').val($(this).parent().parent()[0].id);
        $('#form-actions').trigger('submit');
    });
    $('.arrange').on('click', function(){
        $('#input-arrange').val($(this).parent().parent()[0].id);
        $('#form-arrange').trigger('submit');
    });
    $('#search-input').on('keyup', function(){
        text = $('#search-input').val().toUpperCase();
        $('#board .collection').hide();
        $('#board .collection:contains(' + text + ')').show();
    });    
    $('#search-close').on('click', function(){
        $('#search-input').val('');
        $('#board .collection').show();
    });
});

function arrange(id){
    $('#input-arrange').val(id);
    $('#form-arrange').trigger('submit');
}

function accept(id){
    $('#input-accept').val(id);
    $('#form-accept').trigger('submit');
}

function reject(id){
    $('#input-reject').val(id);
    $('#form-reject').trigger('submit');
}


$(function(){
    $('#btn-edit').on('click', function(){
        $('#form-edit').trigger('submit');
    });
})