$(function(){
    $('#btn-erase').on('click', function(){
        $('#form-erase').trigger('submit');
    });
    $('#btn-edit').on('click', function(){
        $('#form-edit').trigger('submit');
    });
    $('#form-erase').on('submit', function(e){
        r = confirm('¿Realmente desea borrar este registro de manera permanente?');
        if(!r)
            e.preventDefault();
    })
})