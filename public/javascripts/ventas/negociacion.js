console.log("script cargado");
// interes extra es el interes que grava cuando hay una cuota extra en  gerencia puede decidir si se aplica o no 
var datos=[
    {tipo:"Ninguno",forma_pago:"Ninguno",fecha:$.now(),n_cuenta:"",n_documento:"",entidad_financiera:"",valor:"", interes_extra:true},
    {tipo:"Ninguno",forma_pago:"Ninguno",fecha:$.now(),n_cuenta:"",n_documento:"",entidad_financiera:"",valor:"", interes_extra:true},
    {tipo:"Ninguno",forma_pago:"Ninguno",fecha:$.now(),n_cuenta:"",n_documento:"",entidad_financiera:"",valor:"", interes_extra:true},
    {tipo:"Ninguno",forma_pago:"Ninguno",fecha:$.now(),n_cuenta:"",n_documento:"",entidad_financiera:"",valor:"", interes_extra:true},
    {tipo:"Ninguno",forma_pago:"Ninguno",fecha:$.now(),n_cuenta:"",n_documento:"",entidad_financiera:"",valor:"", interes_extra:true},
    {tipo:"Ninguno",forma_pago:"Ninguno",fecha:$.now(),n_cuenta:"",n_documento:"",entidad_financiera:"",valor:"", interes_extra:true},


]
$.post('/api/obtener-negociacion',{cod:$('#cod').val()},function(dt)
	  {
	if(dt!="")
		{
			datos=JSON.parse(dt)
			$("#clase").jsGrid("destroy");
			crear_tabla();
		}
	else
		{
			
		}
});




$( document ).ready(function() {
	
	
   var MyDateField = function(config) {
    jsGrid.Field.call(this, config);
	 
	   
	   ///calendario en español
	  
    $('.datepicker').datepicker({
      dateFormat: 'dd/mm/yy',
      showButtonPanel: false,
      changeMonth: false,
      changeYear: false,
      /*showOn: "button",
      buttonImage: "images/calendar.gif",
      buttonImageOnly: true,*/
      minDate: '+1D',
      maxDate: '+3M',
      inline: true
    });
	   
	
	   
 
 
};
 
	var DateField = function(config) {
    jsGrid.Field.call(this, config);
};

DateField.prototype = new jsGrid.Field({
    sorter: function(date1, date2) {
        return new Date(date1) - new Date(date2);
    },    
    
    itemTemplate: function(value) {
        return new Date(value).toDateString();
    },
    
    filterTemplate: function() {
        var now = new Date();
        this._fromPicker = $("<input>").datepicker({ defaultDate: now.setFullYear(now.getFullYear() - 1) });
        this._toPicker = $("<input>").datepicker({ defaultDate: now.setFullYear(now.getFullYear() + 1) });
        return $("<div>").append(this._fromPicker).append(this._toPicker);
    },
    
    insertTemplate: function(value) {
        return this._insertPicker = $("<input>").datepicker({ defaultDate: new Date() });
    },
    
    editTemplate: function(value) {
        return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
    },
    
    insertValue: function() {
        return this._insertPicker.datepicker("getDate").toISOString();
    },
    
    editValue: function() {
        return this._editPicker.datepicker("getDate").toISOString();
    },
    
    filterValue: function() {
        return {
            from: this._fromPicker.datepicker("getDate"),
            to: this._toPicker.datepicker("getDate")
        };
    }
});

jsGrid.fields.date = DateField;
	
	//*************
	
	$('#clase').empty();
	
	
	crear_tabla();
});

saldo();

$('#meses').change(function(){
	calcular_cuota();
})


function crear_tabla(){
	$("#clase").jsGrid({
    width: "100%",
    height: "400px",
    filtering: false,
    editing: true,
    sorting: true,
    paging: true,
	inserting: true,
	onItemUpdated: function(){
		saldo();
		calcular_cuota();
		console.log('actualizando...');
	},
	invalidMessage: "Los campos ingresados son incorrectos, asegurate que la calificacion sea de A-E y en letra Mayúscula",
    data: datos,
    fields:[
    {name:"tipo", title:"Tipo de Cuota", type:"select", items:["Entrada","Cuota Extraordinaria",]},
    {name:"foma_pago", title:"Forma de Pago", type:"select", items:["Efectivo","Cheque","Transferencia","Cheque PostFechado","Poliza","Crédito Directo"]},
	{name:"fecha", title:"fecha",type:"date"},
	{name:"n_cuenta", title:"Número de Cuenta",type:"text"},
	{name:"n_documento", title:"Número de Documento",type:"text"},
	{name:"entidad_financiera", title:"Entidad Financiera",type:"text"},
	{name:"valor", title:"Valor",type:"number", },
	{ type: "control" }
]
  })}






function interes_extra(){
	var dias_transcurridos=0;
    var saldo_inicial=valor_sin_entrada();
    var i_extra=0;
	datos.forEach(function(element, index){
		if(element.tipo==1)
			{
				console.log("Hay couta extra");
				f_ini = new Date($.now());
				f_fin = new Date(element.fecha);
				var timeDiff = Math.abs(f_fin.getTime() - f_ini.getTime());
				var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
				console.log("dias de diferencia de la cuota extra")
                    console.log((diffDays-dias_transcurridos));

                if($('#dispositivo').prop('checked') && $('#seguro').prop('checked'))
				{
					
                    i_extra += saldo_inicial*(0.1599/360)*(diffDays-dias_transcurridos)
					console.log("interes extra");
					console.log(saldo_inicial*(0.1599/360)*(diffDays-dias_transcurridos));
					console.log("Honorarios extra");
					console.log(honorarios(saldo_inicial,(diffDays-dias_transcurridos), 0.1599));
					i_extra += honorarios(saldo_inicial,(diffDays-dias_transcurridos), 0.1599);
                    saldo_inicial += element.valor;
					
		
				}
			//si -no
			if($('#seguro').prop('checked') && !$('#dispositivo').prop('checked'))
				{
					console.log("dias de diferencia");
                    console.log(diffDays);
                    i_extra += saldo_inicial*(0.1799/360)*diffDays
					i_extra += honorarios(saldo_inicial,diffDays, 0.1799);
                    saldo_inicial += element.valor;
                    console.log("interes extra generado")
                    console.log(i_extra);
				}
			// no -si
			if(!$('#seguro').prop('checked') && $('#dispositivo').prop('checked'))
				{
					console.log("dias de diferencia");
                    console.log(diffDays);
                    i_extra += saldo_inicial*(0.2/360)*diffDays;
					i_extra += honorarios(saldo_inicial,diffDays,0.2);
                    saldo_inicial += element.valor;
                    console.log("interes extra generado")
                    console.log(i_extra);
				}
			//no -no 
			if(!$('#dispositivo').prop('checked') && !$('#seguro').prop('checked'))
				{
					
					i_extra += saldo_inicial*(0.22/360)*(diffDays-dias_transcurridos)
					interes =saldo_inicial*(0.22/360)*(diffDays-dias_transcurridos);
					console.log("valor amortizado.....");
					console.log(saldo_inicial);
					console.log("interes extra.....");
					console.log(saldo_inicial*(0.22/360)*(diffDays-dias_transcurridos));
					console.log("Honorarios extra....");
					console.log(honorarios(saldo_inicial,(diffDays-dias_transcurridos), 0.22));
					comision=honorarios(saldo_inicial,(diffDays-dias_transcurridos), 0.22);
					i_extra += honorarios(saldo_inicial,(diffDays-dias_transcurridos), 0.22);
                    saldo_inicial -= (element.valor-interes-comision);
                    
				}
                //
				dias_transcurridos=+diffDays;
			}
		
		else{
			console.log("no hay cuotas extra")
		}
         
	})
     //return parseFloat(i_extra);
     return parseFloat(i_extra);
}
function valor_sin_entrada(){
	var subtotal=0;
	datos.forEach(function(element){
		if(element.tipo==0)
			{
				subtotal+=element.valor;	
			}
	})
	if($('#seguro').prop('checked'))
		{
			subtotal-=seguro();
		}
	if($('#dispositivo').prop('checked'))
		{
			subtotal -= 590;
		}
	
	
	var amortizar= (($('#valor').val())-subtotal).toFixed(2);
	
	return (parseFloat(amortizar))
}
function saldo(){
	//valor aparente del saldo 
	var subtotal=0;
	datos.forEach(function(element){
		subtotal+=element.valor;
	})
	$("#total").empty(); 
	if($('#seguro').prop('checked'))
		{
			subtotal-=seguro();
		}
	if($('#dispositivo').prop('checked'))
		{
			subtotal -= 590;
		}
	
	
	var amortizar= (($('#valor').val())-subtotal).toFixed(2);
	$("#total").append(amortizar);
	
	return (parseFloat(amortizar))
}
function seguro(){
	var tasa_todo = 3.6/100;
	if($('#valor_auto')>39990)
		{
			tasa_todo=3.1/100;
		}
	var derecho_emision=0.5;
	var prima= $("#valor_auto").val()*tasa_todo;
	if(prima > 250 && prima <=500)
		{
			derecho_emision=1;
		}
	if(prima > 500 && prima <=1000)
		{
			derecho_emision=3;
		}
	if(prima > 1000 && prima <=2000)
		{
			derecho_emision=5;
		}
	if(prima > 2000 && prima <= 4000)
		{
			derecho_emision=7;
		}
	if(prima > 4000)
		{
			derecho_emision=9;
		}
	
	var impuesto_agrario = prima*0.5/100;
	var super_bancos =prima*0.035;
	var iva = (prima+super_bancos+derecho_emision+impuesto_agrario)*0.12;
	var total_seguro =(prima+super_bancos+derecho_emision+impuesto_agrario + iva).toFixed(2);
	$('#valor_seguro').empty();
	$('#valor_seguro').append(total_seguro);
	return (prima+super_bancos+derecho_emision+impuesto_agrario + iva)
	
}
function interes_global(){
	if($('#dispositivo').prop('checked') && $('#seguro').prop('checked'))
				{
					return 0.1599;
				}
			//si -no
			if($('#seguro').prop('checked') && !$('#dispositivo').prop('checked'))
				{
					
					return 0.18;
				}
			// no -si
			if(!$('#seguro').prop('checked') && $('#dispositivo').prop('checked'))
				{
					return 0.2;
				}
			//no -no 
			if(!$('#dispositivo').prop('checked') && !$('#seguro').prop('checked'))
				{
					return 0.22;
				}
}
function calcular_cuota(){
	if($('#meses').val()>0)
		{
			console.log($('#meses').val());
			//aqui se genera el bucle 
			var saldo1 =saldo()+ interes_extra();
            console.log("Valor real a amortizar")
            console.log(saldo1)
			console.log("Tasa de Interes")
            console.log(interes_global());
			var cuota_a=((saldo1)+(saldo1*(interes_global()/360)*dias_prestamo()))/$('#meses').val();
			var cuota = calcular_ajuste(cuota_a);
			$('#cuota').empty();
			$('#cuota').append(cuota.toFixed(2));	
			uri1= JSON.stringify(pagos);
			$('#uri').val(uri1)
		}
	else
		{
			$('#cuota').empty();
			$('#cuota').append("--");
		}
}
$('#dispositivo').change(function(){
 calcular_cuota();
	saldo();
	if($('#dispositivo').prop('checked'))
		{
			$('#valor_dispositivo').empty();
			$('#valor_dispositivo').append("590");
			
		}
	else
		{
			$('#valor_dispositivo').empty();
			$('#valor_dispositivo').append("--");
		}
})
$('#seguro').change(function(){
 calcular_cuota();
	seguro();
	saldo();
})
$(function() {
    $('#toggle-two').bootstrapToggle({
      on: 'Enabled',
      off: 'Disabled'
    });
	 
	 //calendario espańol
 $.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '<Ant',
    nextText: 'Sig>',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
  };
  $.datepicker.setDefaults($.datepicker.regional['es']);
	
	 
	 
	 
	 
	 
  })
$('#reservado').change(function(){
	if($('#reservado').prop('checked'))
		{
			alert("Recuerda se debe serservar con al menos 500  tienen una duracion de 15 días")
		}
})
function n_cuotas_extra(){
	var n=0;
	datos.forEach(function(element){
		if(element.tipo==1)
			{
				n++;
			}
	})
	return n
}
function total_pagar(){
			var saldo1 =saldo();
			var cuota=((saldo1)+(saldo1*(interes_global()/360)*dias_prestamo()));
			return cuota;
}
function interes_honorario(){
	var total=total_pagar();
	var valor_inicial = saldo();
	var indicador =(Math.pow((total/valor_inicial),(1/12))-1)*12;
	return parseFloat(indicador.toFixed(2))+0.0058;
	
}
function interes_compuesto(valor,dias){
	var interes = interes_honorario();
	console.log(interes);
	var aux =(valor*dias/360)*interes;
	return aux;
}
function honorarios(valor,dias,tasa){
	var int_compuesto = interes_compuesto(valor,dias);
	var interes = (valor*dias*tasa/360);
	//valor incrementado 
	return (int_compuesto-interes);
}
var pagos =[];
function tabla(cuota){
	pagos=[];
	var total_capital=0
	var residuo=0;
	var saldo1 =saldo()+ interes_extra();
	var cuota_fin=cuota;
	//genera un JSON que contienenel detalle de cada pago 
	var valor_auto = parseFloat($('#valor_auto').val());
	var last_date_index=0;
	var capital=valor_sin_entrada();
	datos.forEach(function(element,index){
		if(element.tipo==1 || element.tipo==0){
			if(element.tipo==0)
				{
					aux={
						tipo:"Entrada",
						valor:element.valor,
						fecha:element.fecha,
						interes:0,
						honorarios:0,
						capital:element.valor,
						saldo_capital:capital
					}
					pagos.push(aux);
					fecha_entrada=index;
				}
			if(element.tipo==1)
				{
					f_ini = new Date(datos[index-1].fecha);
					f_fin = new Date(element.fecha);
					var timeDiff = Math.abs(f_fin.getTime() - f_ini.getTime());
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
					var honorarios1 = honorarios(capital,diffDays,interes_global());
					var interes=diffDays*(interes_global()/360)*capital;
					var p_capital=(parseFloat(element.valor)-honorarios1-interes);
					aux={
						tipo:"Cuota Extra",
						valor:element.valor,
						fecha:element.fecha,
						interes:interes.toFixed(2),
						honorarios:honorarios1.toFixed(2),
						capital:p_capital.toFixed(2),
						saldo_capital:(capital-p_capital).toFixed(2),
						dias:diffDays
					}
					pagos.push(aux);
					total_capital+=p_capital
					capital -= p_capital;
					last_date_index=index
				}
		}	
	})
	var last_date= new Date(datos[last_date_index].fecha);
	f_ini = new Date($.now());
	var timeDiff = Math.abs(last_date.getTime() - f_ini.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
	f_actual2=new Date($.now());
	f_actual2.setDate(f_actual2.getDate() + diffDays);
	console.log("fecha en la que inicia el prestamo")
	console.log(f_actual2);
	var dias_transcurridos = diffDays;
	//cracion del vector 
	var count =[];
	for(i=0; i<parseFloat($('#meses').val()); i++){
		count.push(i);
	}
	
	var residuo=1
	count.forEach(function(element){
		f_actual2.setDate(f_actual2.getDate() + 30);
		console.log("fecha mas 30")
		console.log(f_actual2)
		
			if(f_actual2.getDay()==0)
				{
					f_actual2.setDate(f_actual2.getDate() + 1);
					console.log("fecha mas 31")
					console.log(f_actual2)
					dias_transcurridos+=31;
					var honorarios1 = honorarios(capital,31,interes_global());
					var interes=31*(interes_global()/360)*capital;
					var p_capital=(cuota_fin-honorarios1-interes).toFixed(2);
					aux={
					tipo:"Pago de Crédito",
					valor:cuota_fin.toFixed(2),
					fecha:f_actual2.toString(),
					interes:interes.toFixed(2),
					honorarios:honorarios1.toFixed(2),
					capital:p_capital,
					saldo_capital:(capital-p_capital).toFixed(2),
					dias:dias_transcurridos
					}	
					pagos.push(aux);
				}
			else
				{
					dias_transcurridos+=30;
					var honorarios1 = honorarios(capital,30,interes_global());
					var interes=30*(interes_global()/360)*capital;
					var p_capital=(cuota_fin-honorarios1-interes);
					aux={
					tipo:"Pago de Crédito",
					valor:cuota_fin.toFixed(2),
					fecha:f_actual2.toString(),
					interes:interes.toFixed(2),
					honorarios:honorarios1.toFixed(2),
					capital:p_capital.toFixed(2),
					saldo_capital:(capital-p_capital).toFixed(2),
					dias:dias_transcurridos
				}	
					pagos.push(aux);
				}
			total_capital+=p_capital
			residuo =capital-p_capital;
			capital -= p_capital;	
	})
//	console.log("Total del Pago Al capital")
//	console.log(total_capital);
	return residuo;
}

function calcular_ajuste(cuota){
	//ajusta la cuota de tal manera que el resultado de la tabla se aproxime a 0 
	var nueva_cuota= cuota;
	var residuo= tabla(nueva_cuota);
	while(residuo>=1.5 || residuo<=-1.5){
		if(residuo >=1.5)
        {
			nueva_cuota+=0.01;
			residuo=(tabla(nueva_cuota));
		} 
		if(residuo <=-1.5)
        {
			nueva_cuota-=0.01;
			residuo=(tabla(nueva_cuota));
		} 
		console.log(residuo)
		if(residuo >-1.5 && residuo <1.5)
        {
			return nueva_cuota;
} 
}
}
function dias_prestamo(){
	//Especificacion 
	//metodo usado para calcular los dias exactos entre lunes y sabado del prestamo
	var last_date_index=0;
	datos.forEach(function(element,index){
		if(element.tipo==1 || element.tipo==0){
			last_date_index=index;
		}	
	})
	//calculos de dias de la cuota extraordinaria
	var	last_date= new Date(datos[last_date_index].fecha);
	f_ini = new Date($.now());
	//f_fin = new Date(element.[last_date]);
	var timeDiff = Math.abs(last_date.getTime() - f_ini.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
	
	f_actual1=new Date($.now());
	f_actual1.setDate(f_actual1.getDate() + diffDays);
	var dias_transcurridos = 0;
	
	for(count=0;count<$('#meses').val();count++)
		{
			f_actual1.setDate(f_actual1.getDate() + 30);
			if(f_actual1.getDay()==0)
				{
					f_actual1.setDate(f_actual1.getDate() + 1);
					dias_transcurridos+=31;
				}
			else
				{
					dias_transcurridos+=30;
				}
			
			
		}
	
	console.log(dias_transcurridos);
	return dias_transcurridos;
	
}

$('#guardar_negociacion').click(function(){
	var data_g =JSON.stringify(datos);
	$.post('/api/guardar-negociacion',{data:data_g, cod:$('#cod').val()},function(dt){
		console.log(dt);
	})
})
/*honorarios
saldo de capital * 38,480% /360 * dias_transcurridos 
czuniga@tecnovatel.com
*/