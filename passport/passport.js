var LocalStrategy = require('passport-local').Strategy;

var bcrypt = require('bcryptjs');

var usuario_model = require('../models/usuario');

var mongoose=require('mongoose');

module.exports = function(passport){
    
    passport.serializeUser(function(user, done){
        done(null, user);
    });
    
    passport.deserializeUser(function(obj, done){
        done(null, obj);
    });
    
    passport.use(new LocalStrategy({
        passReqToCallback : true
    }, function(req, email, password, done){

      
       usuario_model.find({correo:email},function(err,docs){
           console.log(err)
           console.log(docs)
           if(err)
           {
               console.log(err)
               return done(null, false);
           }
           else
           {
            if(docs.length>0)
           {
                var user = docs[0];
                
                if(bcrypt.compareSync(password, user.password)){
                    return done(null, {
                        id: user._id,
                        nombre : user.nombre,
                        email : user.correo,
                        tipo : "1",
                        foto:user.foto
                    });
                } 
                else
                {
                    return done(null, false);
                }  
            }
            else
            {
                return done(null,false)
            }
           }
           

           

       })
        // db.query('select * from usuario inner join agencia on agencia = id_agencia inner join empresa on empresa = id_empresa WHERE  correo_usuario = ?', email, function(err, rows, fields){
            
        //     if(err){
        //         console.log(err);
        //     }                
            
        //     db.end();

		// 	if(rows.length > 0){
                
        //         var user = rows[0];
                
        //         if(bcrypt.compareSync(password, user.password)){
        //             return done(null, {
        //                 id: user.id_usuario,
        //                 nombre : user.nombre_usuario,
        //                 email : user.correo_usuario,
        //                 tipo : user.tipo,
        //                 id_grupo : user.agencia,
        //                 grupo : user.nombre_agencia,
        //                 id_empresa : user.id_empresa,
        //                 empresa : user.nombre_empresa,
        //                 foto:user.foto
        //             });
        //         }   
        //     }
            
        //     return done(null, false);
        
        // });
    }));
}