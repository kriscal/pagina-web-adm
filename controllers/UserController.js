var mongoose = require ('mongoose');
var bcrypt = require('bcryptjs');
var usuario_model = require('../models/usuario');
var list_model=require('../models/list')
module.exports = {
    signIn : function(req, res, next){
        if(req.user){
            if(req.user.tipo == 1){
                list_model.find({activo:1},function(err,docs){
                    return res.render('products/active',{user:req.user, data:docs})
                })
            }
            else{
                if(req.user.tipo == 2){
                    return res.render('public/home',{
                        isAuthenticated : req.isAuthenticated(),
                        user: req.user,
                        title:"Crear Nueva Cuenta"
                    });
                }
                else{
                    return res.render('mymessage', {titulo: "Se ha producido un error", mensaje: 'El usuario no existe'});
                }
            }
        }
        else{

            return res.render('public/home');
        }
    }, 
    
    getUsuarios:function(req,res,next)
    {
        

        usuario_model.find({activo:"1"},function(err,docs){
            if(err)
            {
                return res.redirect('/')
            }
            else
            {
                return res.render('usuarios/usuarios',{
                    isAuthenticated : req.isAuthenticated(),
                    user: req.user,
                    usuarios:docs,
                    msj:req.query.msj
                })
            }
        })
    }, 

    postNuevoUsuario:function(req,res,next){

      
        if(req.body.password==req.body.password2)
        {
            var salt = bcrypt.genSaltSync(10);
            var password = bcrypt.hashSync(req.body.password, salt);
            delete req.body.password2
            delete req.body.password

            req.body.password_usuario=password
            var  usuario = new usuario_model(req.body);
            usuario.save(function(err,docs){
                if(err)
                {
                    console.log(err);
                    return res.redirect('/')
                }
                else
                {
                    return res.redirect('/usuarios')
                }
            })
        }
        else
        {
            res.redirect('/usuarios?msj='+"El Password no Coincide")
        }
    },
    logout: function (req, res, next) {
		req.logout();
		res.redirect('/');
	},

    getBorrarUsuario:function(req,res,next){

        var ObjectId = require('mongoose').Types.ObjectId;
        usuario_model.update({_id:req.query.cod},{activo:0}, function(err,docs){
            if(err)
            {
                console.log(err);
                return res.redirect('/')
            }
            else
            {
                return res.redirect('/usuarios')
            }
        })
    },
    getEditarUsuario: function(req,res,next){

        usuario_model.find({_id:req.query.cod}, function(err,docs){
            if(err)
            {
                console.log(err)
                return res.redirect('/')
            }
            else
            {
                return res.render('usuarios/editar', {
                    isAuthenticated : req.isAuthenticated(),
                    user: req.user,
                    usuario:docs,
                })
            }
        })
    },
    postActualizarUsuario: function(req,res,next){

        var ObjectId = require('mongoose').Types.ObjectId;
        cod=req.body.cod
        delete req.body.cod
        usuario_model.update({_id:cod},req.body, function(err,docs){
            if(err)
            {
                console.log(err);
                return res.redirect('/')
            }
            else
            {
                console.log(docs)
                return res.redirect('/usuarios')
            }
        })
    },

    postActualizarPassword:function(req,res,next){

        var ObjectId = require('mongoose').Types.ObjectId;
        cod=req.body.cod
        delete req.body.cod
        if(req.body.password==req.body.password2)
        {
            var salt = bcrypt.genSaltSync(10);
            var password = bcrypt.hashSync(req.body.password, salt);
            delete req.body.password2
            delete req.body.password
            usuario_model.update({_id:ObjectId(cod)},{password_usuario:password}, function(err,docs){
                if(err)
                {
                    console.log(err);
                    return res.redirect('/')
                }
                else
                {
                    console.log(docs)
                    return res.redirect('/usuarios')
                }
            })  
        }
        else
        {
            res.redirect('/usuarios?msj='+"El Password no Coincide")
        }

        
    },
    logout:function(req,res,next){
        req.logout();
		res.redirect('/');
    }
}