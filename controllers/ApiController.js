
var formidable = require('formidable');
var campana_model = require('../models/campana');
//var csv=require('csvtojson');
var image_model = require('../models/image');
var list_model=require('../models/list')
var path = require('path');
var ObjectId = require('mongoose').Types.ObjectId;
module.exports={
    postSubir:function(req,res,next)
	{
		console.log("********** SUBIDA ************")
		console.log("Se ingreso al modulo de subida");
		var subir = new formidable.IncomingForm();
		var ruta = "/images/productos/";
		var body;
		//console.log(req);
		subir.uploadDir = 'public/images/productos';
		subir.parse(req);
		subir.on('error', function (err) {
			console.log("HUbo un error");
			console.log(err);
		});
		subir.on('end', function (field, file) {
			console.log("Subido Correctamente");
			console.log('archivoimage:'+file);
		});
		subir.on('aborted', function(dat) {
			//console.log(dat);
        });
		subir.on('fileBegin', function (field, file) {
			console.log(file.type)
			if (file.name) {
				var ext;
				if (file.type == 'image/png') {
					ext = ".png"
				}
				if (file.type == 'image/jpeg') {
					ext = ".jpeg"
				}
				if (file.type == 'image/jpg') {
					ext = ".jpg"
                }

				file.path += ext;
				var a = file.path.split('public', 2);
				ruta = a[1];
				console.log("la ruta es ");
				console.log(ruta);
				return res.json(ruta);
				//return ruta;
			}
		});
		subir.on('file', function (field, file) {
			console.log('Archivo recibido');
		});

    },
    postUpdateImage:function(req,res,next){
		console.log("********** SE EJECUTA EL API ************")
		id=req.body.id
		_index=req.body.index
		aux=[]
		console.log(req.body)
		list_model.find({_id:id},function(err,docs){
			console.log(err)
			console.log(docs)
			docs[0].image.forEach(function(el, index){
				if(index==_index)
				{
					aux.push(req.body.data)
				}
				else
				{
					aux.push(el)
				}
			})
			list_model.update({_id:id},{image:aux}, function(err1,docs1){
				console.log(err1)
				return res.json('success')
			})
		})
		
		
	},
    getSaveDB:function(req,res,next){
        var  camp = new campana_model(req.query);
        camp.save(function(err,docs){
            if(err)
            {
                return res.json("error")
            }
            else
            {
                console.log(docs)
                return res.json(docs._id)
            }
        })
    },

    getData:function(req,res,next){
        campana_model.find({_id:req.query.id},function(error,docs){
            var csvFilePath= path.join(__dirname, '../public'+docs[0].archivo);
            console.log(csvFilePath)
            var aux=[], aux2 = [] ;
            csv({noheader:true})
            .fromFile(csvFilePath)
         
            .then((csvRow)=>{ 
                console.log(csvRow) 
                return res.json({
					data:csvRow,
					nombre:docs[0].nombre,
					enlace:docs[0].enlace,
					mensaje:docs[0].mensaje
				})
            })
            

        })
  
		
},

	postUpdateCategory:function(req,res,next){
		index=req.query.index
		data=req.query.data
		cod=req.query.cod
		var aux=[]
		image_model.find({_id:ObjectId(cod)}, function(err,docs){
			console.log(docs[0].category)

			docs[0].category.forEach(function(dt,index1){
				console.log(dt)
				if(index1==index)
				{
					aux.push(data)
				}
				else
				{
					aux.push(dt)
				}
			})
			
			image_model.update({_id:ObjectId(cod)},{
				category:aux		
			},function(err1,docs1){
				if(err1)
				{
					return res.json('error')
				}
				else
				{
					return res.json('success')
				}
			})
		})
		
	}
}