var mongoose = require ('mongoose');
var list_model=require('../models/list')
var product_model=require('../models/product')
var category_model=require('../models/category')
var images_product_model= require('../models/images_product')
module.exports={
    getCategoryProducts:function(req,res,next){
        category_model.find(function(err,docs){
            return res.render('products/category',{user:req.user, data:docs})
        })
    },
    postCategoryProducts:function(req,res,next){
        console.log(req.body)
        var document= new category_model(req.body)
                document.save(function(err2){
                    return res.redirect('/category')
                })
    },
    postInactiveAdd:function(req,res,next){
        var document= new list_model({
                        activo:0,
                        featured:0,
                        min:1,
                        max:1,
                        codigo:req.body.codigo,
                        categoria:req.body.category,
                        descripcion:req.body.descripcion
                })
                document.save(function(err2){
                    return res.redirect('productos-inactivos')
                })
    },
    getAddStock:function(req,res,next){
        product_model.find({codigo:req.query.codigo},function(err,docs){
            list_model.find({_id:req.query._id},function(err,list){
                console.log(docs)
                if(docs.length==0){
                    docs=[{
                        disponibilidad:0
                    }]
                }
            return res.render('products/stock-add',{user:req.user, data:docs,list:list})
        })
    })
    },
    postAddStock:function(req,res,next){
        product_model.find({codigo:req.body.codigo},function(err,docs){
            //console.log("Longitud: "+docs.length)
            list_model.find({_id:req.body._id},function(err,list){ 
            if(docs.length>0){
                disponibilidad={
                    color:req.body.color,
                    codigo_sys:req.body.codigo+"-"+req.body.talla,
                    precio:req.body.precio,
                    stock:req.body.stock,
                    talla:req.body.talla,
                    categoria:list[0].categoria,
                    embalaje:req.body.embalaje
                }
                product_model.findByIdAndUpdate({_id:docs[0]._id},{$push:{"disponibilidad":disponibilidad}},function(err1,docs1){
                    //console.log(err1)
                    //console.log(docs1)
                    return res.redirect('/producto-add-stock?_id='+req.body._id+'&codigo='+req.body.codigo)
                  })
            }
            else{
                    var document= new product_model({
                                    codigo:req.body.codigo,
                                    disponibilidad:[{
                                        color:req.body.color,
                                        codigo_sys:req.body.codigo+"-"+req.body.talla,
                                        precio:req.body.precio,
                                        stock:req.body.stock,
                                        talla:req.body.talla,
                                        categoria:list[0].categoria,
                                        embalaje:req.body.embalaje,
                                    }],
                                    categoria:list[0].categoria,
                                    descripcion:list[0].descripcion
                            })
                            document.save(function(err2){
                                return res.redirect('/producto-add-stock?_id='+req.body._id+'&codigo='+req.body.codigo)
                    })
            }
        })
    })        
    },
    getActiveProducts:function(req,res,next){
        list_model.find({activo:1},function(err,docs){
            return res.render('products/active',{user:req.user, data:docs})
        })
    },
    getInactiveProducts:function(req,res,next){
        list_model.find({activo:0},function(err,docs){
            category_model.find(function(err,category){
            return res.render('products/inactive',{user:req.user, data:docs,category:category})
        })
        })
    }, 
    getDisable:function(req,res,next){
        list_model.update({_id:req.query._id},{activo:0}, function(err,docs){
            return res.redirect('/productos-activos')
        })
    }, 
    getColorsCod:function(req,res,next){
        product_model.find({codigo:req.query.codigo},function(err,docs){
           var colores=[]
           docs.forEach(function(el){
               el.disponibilidad.forEach(function(el1){
                colores.push(el1.color)
               })
               
           })
           var colores_unicos=toUnique(colores)
        //    console.log(colores_unicos)
           return res.render('products/color',{colores:colores_unicos, codigo:req.query.codigo})  
        })
    },
    getImage:function(req,res,next){
        images_product_model.find({color:req.query.color,codigo:req.query.codigo},function(err,docs){
            var resultado=docs
            if(resultado.length==0)
            {
                resultado.push({
                    codigo:req.query.codigo
                })
                resultado[0].image=['/img/z2.jpg','/img/z2.jpg','/img/z2.jpg','/img/z2.jpg']	                
            }
            return res.render('products/images',{data:resultado,codigo:req.query.codigo,color:req.query.color})
        })
    },
    postUpdateImage:function(req,res,next){
        //recibe : data(url), codigo, color index
        console.log(req.body)
        images_product_model.find({codigo:req.body.codigo,color:req.body.color}, function(err,docs){
            if(docs.length==0)
            {
                var imagenes=['/img/z2.jpg','/img/z2.jpg','/img/z2.jpg','/img/z2.jpg']	
                imagenes[req.body.index]=req.body.data
                var documento={
                    codigo:req.body.codigo,
                    color:req.body.color,
                    image:imagenes
                }
                var obj= new images_product_model(documento)
                obj.save(function(err2){
                    return res.json("success")
                })
            }
            else
            {
                var aux_img=docs[0].image
                aux_img[req.body.index]=req.body.data
                var documento={
                    codigo:req.body.codigo,
                    color:req.body.color,
                    image:aux_img
                }
                images_product_model.update({codigo:req.body.codigo,color:req.body.color},documento,function(err2,docs){
                    console.log(docs)
                    return res.json("success")
                })
            }
        })

    },
    getProductStock:function(req,res,next){
        product_model.find({codigo:req.query.cod})
        .sort({'fecharegistro':-1})
        .limit(1)
        .exec(function(err,docs){
            return res.render('products/stock',{data:docs[0]})
        })
        
    },
    getEnable:function(req,res,next){
        list_model.update({_id:req.query._id},{activo:1}, function(err,docs){
            return res.redirect('/productos-inactivos')
        })
    },
    getFeatured:function(req,res,next){
        list_model.update({_id:req.query._id},{featured:1}, function(err,docs){
            console.log(docs)
            return res.redirect('/productos-activos')
        })
    },
    getUnfeatured:function(req,res,next){
        list_model.update({_id:req.query._id},{featured:0}, function(err,docs){
            console.log(docs)
            return res.redirect('/productos-activos')
        })
    }
}



function toUnique(a, b, c) { 
    b = a.length;
    while (c = --b)
      while (c--) a[b] !== a[c] || a.splice(c, 1);
    console.log("Codigos unicos "+ a.length)
    return a 
  }