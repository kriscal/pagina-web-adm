var product_model= require('../models/product')
var lista_model= require('../models/list')
var Request = require("request")
var express = require('express');
var router = express.Router();

var soap=require('soap')
var parseString = require('xml2js').parseString;


module.exports= { 
    actualizacion:function(req,res,next){

      //**proceso de actualizacion de productos
      /*
      1.conexion con el webservice y obtener los codigos unicos 
      2. obtener data de la base de mongo 
      3. comparar los codigos unicos con los codigos recibidos de mongo

      //asignacion

      4. si el codigo unico  no existe en mongo este arreglo sera almacenado en la variables nuevos_codigos
      5. si el codigo existe  
      */
      var codigo=req.query.cod
      //var url = 'http://190.63.19.34:8082/Sistema.asmx?WSDL'
      var url = 'http://192.168.1.151:8082/Sistema.asmx?WSDL'
                        
                        
                        soap.createClient(url, function(err, client) {
                          console.log(err)
                        client.maoecuador(function (err1, result) {     
                         console.log(err1)
                         
                         var data_imagen={}
                         
                         parseString(result.maoecuadorResult, function (err2, result1) {
                     
                          console.log(err2)
                         var resultado=[];
                         var productos_nuevos=[];
                         var codigos=[];
                         var productos_nuevos=[]
                         var respuesta=result1.Xml.Producto

                          //se carga la variables codigos
                          respuesta.forEach(function(el) {
                            // console.log(el)
                            var aux=(el.Codigo[0].split("-"))[0]
                            codigos.push(aux)
                          }) 

                          //la variable codigos unicos guarda los codigos unicos  de el inventario
                          var codigos_unicos=toUnique(codigos)

                          //recorre todos los codigos unicos 
                          codigos_unicos.forEach(function(codigo){
                            var dta=getObjectByCode(respuesta,codigo)
                            resultado.push({
                              codigo:codigo,
                              disponibilidad:dta.disponibilidad,
                               categoria:dta.disponibilidad[0].categoria,
                               marca:dta.disponibilidad[0].marca,
                               genero:dta.disponibilidad[0].genero,
                               descripcion:dta.disponibilidad[0].descripcion,
                            })
                          })
                          
                          product_model.collection.drop(function(err, docs){
                            console.log(err)
                            console.log(docs)
                            product_model.insertMany(resultado, function(err1,docs1){
                              return res.json(resultado)
                            })
                          })

                         });
                      
                        });
                        
                      });
    
    },

    actualizacionList:function(req,res,next){

      //**proceso de actualizacion de productos
      /*
      1.conexion con el webservice y obtener los codigos unicos 
      2. obtener data de la base de mongo 
      3. comparar los codigos unicos con los codigos recibidos de mongo

      //asignacion

      4. si el codigo unico  no existe en mongo este arreglo sera almacenado en la variables nuevos_codigos
      5. si el codigo existe  
      */
      var codigo=req.query.cod
      //var url = 'http://190.63.19.34:8082/Sistema.asmx?WSDL'
      var url = 'http://192.168.1.151:8082/Sistema.asmx?WSDL'
                        
                        
                        soap.createClient(url, function(err, client) {
                          console.log(err)
                        client.maoecuador(function (err1, result) {     
                         console.log(err1)
                         
                         var data_imagen={}
                         
                         parseString(result.maoecuadorResult, function (err2, result1) {
                     
                          console.log(err2)
                         var resultado=[];
                         var productos_nuevos=[];
                         var codigos=[];
                         var productos_nuevos=[]
                         var respuesta=result1.Xml.Producto

                          //se carga la variables codigos
                          respuesta.forEach(function(el) {
                            // console.log(el)
                            var aux=(el.Codigo[0].split("-"))[0]
                            codigos.push(aux)
                          }) 

                          //la variable codigos unicos guarda los codigos unicos  de el inventario
                          var codigos_unicos=toUnique(codigos)

                          //recorre todos los codigos unicos 
                          codigos_unicos.forEach(function(codigo){
                            var dta=getObjectByCode(respuesta,codigo)
                            resultado.push({
                              codigo:codigo,
                               categoria:dta.disponibilidad[0].categoria,
                               marca:dta.disponibilidad[0].marca,
                               genero:dta.disponibilidad[0].genero,
                               linea:dta.disponibilidad[0].linea,
                               descripcion:dta.disponibilidad[0].descripcion,
                               disponibilidad:dta.disponibilidad
                               
                            })
                          })
                          
                          lista_model.collection.drop(function(err, docs){
                            console.log(err)
                            console.log(resultado)
                            lista_model.insertMany(resultado, function(err1,docs1){
                              return res.json(resultado)
                            })
                          })

                         });
                      
                        });
                        
                      });
    
    }

}
     
function toUnique(a, b, c) { 
    b = a.length;
    while (c = --b)
      while (c--) a[b] !== a[c] || a.splice(c, 1);
    //console.log("Codigos unicos "+ a.length)
    return a 
  }

  function getObjectByCode(respuesta, codigo){
    //Respuesta es la data sin pricesar del webservice 
    //codigo es el codigo que se desea devolver la data 

  var aux_disponibilidad=[]
  var marca, cod_marca, categoria, cod_categoria, genero, cod_genero

  respuesta.forEach(function(resp){
      var x=resp.Codigo[0].split("-")[0]
      if(x==codigo)
      {
        codigo=codigo
        marca=resp.desmar[0]

        cod_marca=resp.codmar[0]
        
        linea=resp.deslin[0]
        cod_linea=resp.codlin[0]
        
        categoria=resp.descat[0]
        cod_categoria=resp.codcat[0]

        genero=resp.desgen[0]
        cod_genero=resp.codgen[0]
        descripcion=resp.Descripcion[0]

        aux_disponibilidad.push(
          {
            color:resp.Color[0],
            codigo_sys:resp.Codigo[0],
            precio:resp.Precio[0],
            stock:resp.Stock[0],
            talla:resp.Codigo[0].split("-")[1],
            marca:marca,
            categoria:categoria,
            genero:genero,
            linea:linea,
            descripcion:descripcion
          }
        )
      }
  })

  return ({
    codigo:codigo,
    marca:marca,
    cod_marca:cod_marca,
    categoria:categoria,
    cod_categoria:cod_categoria,
    genero:genero,
    cod_genero:cod_genero,
    disponibilidad:aux_disponibilidad
  })


}