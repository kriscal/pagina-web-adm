image_model=require('../models/images')
module.exports={
    getBanner:function(req,res,next){
        image_model.find({type:"BANNER"},function(err,docs){
            return res.render('images/banner',{
                images:docs
            })
        })
    }, 
    getSetBannerImage:function(req,res,next){
        req.query.type="BANNER"
        var imagen= new image_model(req.query)
        imagen.save(function(err,docs){
            if(err)
            {
                return res.json("error")
            }
            else
            {
                return res.json('success')
            }
        })
    }
}