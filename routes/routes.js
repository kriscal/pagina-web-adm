var express = require('express');
var router = express.Router();
var passport = require('passport');
var controllers = require('.././controllers');
var AuthMiddleware = require('.././middleware/auth');

router.post('/', passport.authenticate('local', {
	successRedirect : '/',
	failureRedirect : '/',
	failureFlash : true 
}));

//subir Imagen 
router.post('/api/subir-imagen', controllers.ApiController.postSubir)
router.post('/api/update-imagen', controllers.ApiController.postUpdateImage)

router.get('/', controllers.UserController.signIn);

//Actualizar Base de datos
router.get('/update',controllers.wsmao.actualizacionList);
//Actualiza el stock de la tienda 
router.get('/update-stock',controllers.wsmao.actualizacion);
//Categorias
router.get('/category',controllers.ProductController.getCategoryProducts);
router.post('/category',controllers.ProductController.postCategoryProducts);
//GEstion de Productos 
router.post('/producto-nuevo',controllers.ProductController.postInactiveAdd);
router.get('/producto-add-stock',controllers.ProductController.getAddStock);
router.post('/producto-add-stock',controllers.ProductController.postAddStock);
router.get('/productos-activos', controllers.ProductController.getActiveProducts)
router.get('/productos-inactivos', controllers.ProductController.getInactiveProducts)
router.get('/product-disable', controllers.ProductController.getDisable)
router.get('/product-enable', controllers.ProductController.getEnable)
router.get('/product-stock', controllers.ProductController.getProductStock)
router.get('/product-featured', controllers.ProductController.getFeatured)
router.get('/product-unfeatured', controllers.ProductController.getUnfeatured)
//imagenes
router.get('/product-image',controllers.ProductController.getColorsCod)
router.get('/product-image-color',controllers.ProductController.getImage)
router.post('/actualizar-foto', controllers.ProductController.postUpdateImage)

router.get('/edit-banner',controllers.ImageController.getBanner)
router.get('/actualizar-banner', controllers.ImageController.getSetBannerImage)
//router.get('/categorias', controllers.ProductController.getCategories)


//USUARIO 
//usuarios

router.get('/usuarios', AuthMiddleware.isLogged, controllers.UserController.getUsuarios)
router.post('/nuevo-usuario', AuthMiddleware.isLogged, controllers.UserController.postNuevoUsuario)
router.get('/borrar-usuario', AuthMiddleware.isLogged, controllers.UserController.getBorrarUsuario)
router.get('/editar-usuario', AuthMiddleware.isLogged, controllers.UserController.getEditarUsuario)
router.post('/actualizar-usuario', AuthMiddleware.isLogged, controllers.UserController.postActualizarUsuario)
router.post('/actualizar-password', AuthMiddleware.isLogged, controllers.UserController.postActualizarPassword)
router.get('/salir', AuthMiddleware.isLogged,controllers.UserController.logout)


module.exports = router;