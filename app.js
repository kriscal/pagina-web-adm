var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var session = require('express-session');

var flash = require('connect-flash');
var passport = require('passport');
var routes = require('./routes/routes');
var formidable = require('express-formidable');
require('./passport/passport')(passport);

var mongoose = require('mongoose');
//mongoose.connect('mongodb://localhost/maounderwear', function(){
  mongoose.connect('mongodb://34.66.102.221:5000:27020/ecommerce', { useNewUrlParser: true });


//process.setMaxListeners(1000);

var app = express();

app.locals.moment = require('moment');
app.use(cookieParser());
app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized : false
}));

app.use(flash());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());
app.use(passport.session());
app.use(function(req, res, next){
    res.header('Access-Control-Allow-Origin',"*");
    res.header('Access-Control-Allow-Methods','GET, PUT, POST, DELETE');
    res.header('Access-Control-Allow-Headers','Content-Type');
    next();
});
app.use('/', routes);
app.use(formidable({
    keppExtensions : true
}));

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


app.use(function(err, req, res, next) {    
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
    
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;




