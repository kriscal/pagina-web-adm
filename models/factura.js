var mongoose = require ('mongoose');
	Schema = mongoose.Schema;

var facturaSchema = new Schema({
    cuenta:[],
	id_comercio:{type:String},
	token:{type:String},
	info_tributaria:{
		razonsocial:{type:String},
		ruc:{type:String},
		nombrecomercial:{type:String},
		direccion:{type:String}
	},
	info_factura:{
		comprador:{type:String},
		ruc:{type:String},
		direccion:{type:String},
	},
	productos:[],
	info_adicional:{
		telefono:{type:String},
		email:[]
	}
})

module.exports = mongoose.model('Factura', facturaSchema);



/*
var  data={
	id_comercio:"3",
	token:"sjjhs&%$",
	info_tributaria:{
		razonsocial:"luis Bedon",
		ruc:"0503646556001",
		nombrecomercial:"smart tech solutions",
		direccion:"Av. Bogotá, la península"
	},
	info_factura:{
		comprador:"Omar Altamirano",
		ruc:"1805029434",
		direccion:"Colinas del Sur, Huachi grande",
	},
	productos:[
        {
			descripcion:"PEDIALITE",
			codigo:"PDTE12",
			catidad:2,
			precio_unitario:3,
			descuento:"10%",
			iva:true,
			ice:false
		},
		{
			descripcion:"Suero Oral",
			codigo:"SUER21",
			catidad:3,
			precio_unitario:2.5,
			descuento:"0%",
			iva:true,
			ice:false
		}
		],
	info_adicional:{
		telefono:"0998374744",
		email:"omar@ecuadorsmart.com"
	}
}
*/