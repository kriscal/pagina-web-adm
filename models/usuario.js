var mongoose = require ('mongoose');
	Schema = mongoose.Schema;

var usuarioSchema = new Schema({

	nombre: {type:String},
	correo: {type:String},
	tipo: {type:String},
	password: {type:String},
	celular: {type:String},
	foto:{type:String},
	activo:{type:String,default:"1"},
	f_creacion: {type:Date, default: Date.now}
})

module.exports = mongoose.model('User', usuarioSchema);