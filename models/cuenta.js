var mongoose = require ('mongoose');
	Schema = mongoose.Schema;

var cuentaSchema = new Schema({
	nombre_cuenta: {type:String},
	tipo: {type:String},
	sitio_web: {type:String},
	email: [String],
	telefono: {type:String},
	descripcion: {type:String},
	sector: {type:String},
	numero_empleados: {type:String},
	ruc: {type:String},
	direccion: {type:String},
	ciudad: {type:String},
	coordenadas: {type:String},
	propietario: {type:String},
	visible: {type:String},
	f_creacion: {type:Date, default: Date.now}
})

module.exports = mongoose.model('Cuenta', cuentaSchema);