var mongoose = require ('mongoose');
	Schema = mongoose.Schema;

var productSchema = new Schema({
	codigo: {type:String},
	fecharegistro:{type:Date, default: Date.now},
	disponibilidad: {type:Object},
	categoria: {type:String},
	genero: {type:String},
	marca: {type:String},
	linea: {type:String},
	descripcion: {type:String},
	
})

module.exports = mongoose.model('productos', productSchema);