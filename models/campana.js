var mongoose = require ('mongoose');
	Schema = mongoose.Schema;

var campanaSchema = new Schema({
	nombre: {type:String},
	archivo: {type:String},
	mensaje: {type:String},
	enlace: {type:String},
	f_creacion: {type:Date, default: Date.now}
})

module.exports = mongoose.model('Campana', campanaSchema);