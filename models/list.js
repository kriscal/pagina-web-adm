var mongoose = require ('mongoose');
	Schema = mongoose.Schema;

var imageSchema = new Schema({
	codigo: {type:String},
	image: {type:Object},
	category:{type:Object},
	descripcion:{type:String},
	activo:{type:Number,default: 0},
	codigo_genero:{type:String},
    genero:{type:String},
    codigo_categoria:{type:String},
    categoria:{type:String},
    codigo_marca:{type:String},
    marca:{type:String},
    codigo_linea:{type:String},
	linea:{type:String},
	featured:{type:Number, default:0},
	disponibilidad: {type:Object},
	min:{type:Number,default: 1.1},
	max:{type:Number,default: 1.1},

})

module.exports = mongoose.model('List', imageSchema);