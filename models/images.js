var mongoose = require ('mongoose');
	Schema = mongoose.Schema;

var imageSchema = new Schema({
	alt: {type:String},
	path:{type:Object},
    type:{type:String},
    date:{type:Date, default: Date.now}
})

module.exports = mongoose.model('Image', imageSchema);