var mongoose = require ('mongoose');
	Schema = mongoose.Schema;

var imageSchema = new Schema({
	codigo: {type:String},
	image: {type:Object},
	description:{type:String},
	activo:{type:Number,default: 1},
	category:{type:Object}

})

module.exports = mongoose.model('imagenes', imageSchema);